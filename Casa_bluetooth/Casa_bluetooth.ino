#include <Servo.h>

//-----------------------------------------------------------------------------------------//
#include <DHT.h>              //Incluimos libreria DHT de adafruit
//-----------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------//
#define DHTPIN 13              //Definimos el sensor de temperatura en el pin digital 2
#define DHTTYPE DHT11         //Declaramos que el sensor es el DHT11
//-----------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------//
DHT dht(DHTPIN, DHTTYPE);     //Inicializamos el sensor DHT11
//-----------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------//
int relay1 = 2;
int relay2 = 9;
int relay3 = 7;
int relay4 = 8;
int sen_vibra = 12;
int sen_gas = 5;
int sen_luz = 4;

int calor = 30;               //Declaramos la variable calor con un valor de 30
int lluvia = 15;            //Declaramos la variable tormenta con un valor de 15
int gas_limite = 250;         //Limite maximo de gas para que se active el Led

int estado= 0;
int estado_gas;
int estado_luz;
int estado_vibra;

Servo servoX;

//char buffer[10];

unsigned long startMillis;  
unsigned long currentMillis;
const unsigned long period = 1000; 

unsigned long startMillis2; 
unsigned long currentMillis2;
const unsigned long period2 = 5000; 

//-----------------------------------------------------------------------------------------//

//-----------------------------------------------------------------------------------------//
void setup() 
{
  Serial.begin(9600);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(relay3, OUTPUT);
  pinMode(relay4, OUTPUT);
  pinMode(sen_vibra, INPUT);
  pinMode(sen_gas, INPUT);
  pinMode(sen_luz, INPUT);
  servoX.attach(3);
  servoX.write(0);

  startMillis = millis(); 
  startMillis2 = millis(); 
  
  dht.begin();                //Empieza el sensor DHT11
}
//-----------------------------------------------------------------------------------------//


//-----------------------------------------------------------------------------------------//
void loop() 
{
  if(Serial.available()>0){       
      estado = Serial.read();
  }

if(estado==0){
digitalWrite(relay1, HIGH);
digitalWrite(relay2, HIGH);
digitalWrite(relay3, HIGH);
digitalWrite(relay4, HIGH);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////Relay1/////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(estado=='a'){
  digitalWrite(relay1, LOW);
}

if(estado=='b'){
  digitalWrite(relay1, HIGH);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////Relay2///////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(estado=='c'){
  digitalWrite(relay2, LOW);
}

if(estado=='d'){
  digitalWrite(relay2, HIGH);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////Relay3/////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(estado=='e'){
  digitalWrite(relay3, LOW);
}

if(estado=='f'){
  digitalWrite(relay3, HIGH);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////Relay4/////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(estado=='g'){
  digitalWrite(relay4, LOW);
}

if(estado=='h'){
  digitalWrite(relay4, HIGH);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////Servomotor/////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(estado=='n'){
  servoX.write(90);
  delay(1000);
}

if(estado=='m'){
  servoX.write(0);
  delay(1000);
}

/*
  int gasi = estado_gas;
  int vibras = estado_vibra;
  //int temper = t;
  sprintf(buffer, "%d,%d,%d",gasi,vibras);//concadena varias variables  diferentes como int, char en una sola cadena, aprovecho la coma para dividir en appinventor los datos
  Serial.println(buffer);
*/
  currentMillis = millis(); 
  if (currentMillis - startMillis >= period)
  {
    gas();
    startMillis = currentMillis; 
  }

  currentMillis2 = millis(); 
  if (currentMillis2 - startMillis2 >= period2) 
  {
    temp();
    startMillis2 = currentMillis2; 
  }

  vibracion();

}
//-----------------------------------------------------------------------------------------//





//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
void gas() {
  
  estado_gas = analogRead(sen_gas);
    
  Serial.print("Nivel de gas: ");
  Serial.println(estado_gas);   //Imprimimos en el monitor serial la lectura del sensor de gas
  Serial.print("\n");
  

  if (estado_gas > gas_limite) 
  {  //Si la lectura del sensor es mayor a el limite maximo
    Serial.println("Fuga de gas detectada. Tomando acciones necesarias");
    digitalWrite(relay1, LOW);
    delay (500);
    digitalWrite(relay1, HIGH);
    delay (500);
    digitalWrite(relay1, LOW);
    delay (500);
    digitalWrite(relay1, HIGH);
    delay (500);
    digitalWrite(relay1, LOW);
    delay (500);
    digitalWrite(relay1, HIGH);
    delay (500);         
  }
 
}
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
void temp() {
  //float h = dht.readHumidity();-------------Guardamos la lectura de humedad en la variable flotante h
  
  float t = dht.readTemperature();         //Guardamos la lectura de temperatura en grados centigrados en la variable flotante t

  //Se comprueba si hay algun error en la lectura
  if (isnan(t)) 
  {
    Serial.println("Error obtenido en los datos del sensor DHT11");
    return;
  }

  if (t >= calor || t <= lluvia) 
  { //Si la temperatura es mayor al calor limite o es menor a la temperatura de la tormenta
    Serial.println("Hace mucho calor. Se recomienda abrir ventanas");    //Se enciende el Led
  }


  Serial.print("Temperatura: ");
  Serial.print(t);
  Serial.println(" °C \n");

}
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
void vibracion(){

  estado_vibra= digitalRead(sen_vibra);

  if(estado_vibra==HIGH){
    Serial.println("Hay terremoto!!!!!");
    digitalWrite(relay1, LOW);
    delay (500);
    digitalWrite(relay1, HIGH);
    delay (500);
    digitalWrite(relay1, LOW);
    delay (500);
    digitalWrite(relay1, HIGH);
    delay (500);
    digitalWrite(relay1, LOW);
    delay (500);
    digitalWrite(relay1, HIGH);
    delay (500);
  }
}
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------//
