#include <DHT.h>              //Incluimos libreria DHT de adafruit

#define DHTPIN 4              //Definimos el sensor de temperatura en el pin digital 4

#define DHTTYPE DHT11         //Declaramos que el sensor es el DHT11

DHT dht(DHTPIN, DHTTYPE);     //Inicializamos el sensor DHT11

int led= 2;                   //Declaramos la variable led en el pin digital 2
int calor= 30;                //Declaramos la variable calor con un valor de 30
int tormenta= 15;             //Declaramos la variable tormenta con un valor de 15

void setup() {
  Serial.begin(115200);         //Iniciamos la comunicacion serial
  pinMode(led, OUTPUT);       //Declaramos led como salida
  dht.begin();                //Empieza el sensor DHT11
}

void loop() {
  delay (5000);    //Metemos 5 segundos de retraso al inicio para obtener lecturas mas exactas                     

  float h = dht.readHumidity();            //Guardamos la lectura de humedad en la variable flotante h

  float t = dht.readTemperature();         //Guardamos la lectura de temperatura en grados centigrados en la variable flotante t

  //float f = dht.readTemperature(true);    Lectura de temperatura en grados Fahrenheit, no tomado en cuenta para este programa

 //Se comprueba si hay algun error en la lectura
  if (isnan(h) || isnan(t)){   
  Serial.println("Error obtenido en los datos del sensor DHT11");
  return;
  }

  /*  Variables de las lecturas de indice de calor, no tomado en cuenta para este programa
  float hic = dht.computeHeatIndex(t, h, false);
  float hif = dht.computeHeatIndex(f, h);
  */

  if(t>calor || t<tormenta){    //Si la temperatura es mayor al calor limite o es menor a la temperatura de la tormenta
    digitalWrite(led, HIGH);    //Se enciende el Led
  }

  else{                         //Si no
    digitalWrite(led, LOW);     //El Led se mantiene apagado
  }

  Serial.print("Humedad: ");
  Serial.print(h);
  Serial.println(" %\t");
  Serial.print("Temperatura: ");
  Serial.print(t);
  Serial.println(" *C ");

}
