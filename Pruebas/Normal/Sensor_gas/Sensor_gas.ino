int gas_limite = 200;      //Limite maximo de gas para que se active el Led
int led = 2;               //Led que simboliza si hay una fuga de gas

void setup() {
  Serial.begin(115200);
  pinMode(led, OUTPUT);
}

void loop() {
  Serial.println(analogRead(A0));   //Imprimimos en el monitor serial la lectura del pin Analogico 0, que es donde esta el sensor

  if (analogRead(A0) > gas_limite){   //Si la lectura del sensor es mayor a el limite maximo
      digitalWrite(led, HIGH);        //Se activa el Led
  }
  else{                               //Si la lectura del sensor no es mayor a el limite maximo
    digitalWrite(led, LOW);           //El Led se mantiene apagado
  }
  delay(300);                         //Retraso de 300 milisegundos
}
